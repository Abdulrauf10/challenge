const {user_game, user_game_data, user_game_history} = require ('../models')

module.exports = {
    addUser: (req, res) => {
        res.render('userAddView')
    },
    saveUser: (req, res) => {
        user_game.create({
            name: req.body.name,
            email: req.body.email,
            credit: req.body.credit
        }).then(userGame  => {
            user_game_data.create({
                user_game_id: userGame.id,
                alamat: req.body.alamat,
                kelamin: req.body.kelamin
            }).then(winer => {
                user_game_history.create({
                    win: req.body.win,
                    lose: req.body.lose
                }).then(biodata => res.redirect(301, '/dashboard'))
            })
        })
    },
    userDelete: (req, res) => {
        const userId = req.params.id;
    
        user_game_data.destroy(({
            where:{
                user_game_id: userId
            }
        })).then(biodata => {
            user_game.destroy({
                where:{
                    id: userId
                }
            }).then(history => {
                user_game_history.destroy({
                    where:{
                        id: userId
                    }
                }).then(view => {
                    res.redirect(301, '/dashboard')
                })
            })
        });
    },
    userUpdateGet: (req, res) => {
        const userId = req.params.id;
    
        user_game.findOne({
            where:{
                id: userId
            }
        }).then(biodata => {
            user_game_data.findOne({
                where:{
                    user_game_id: userId
                }
            }).then(user => {
                user_game_history.findOne({
                    where:{
                        id: userId
                    }
                }).then(execute => {
                    res.render('update', { biodata, user, execute })
                })
            })
        })
    },
    userUpdatePost: (req, res) => {
        const userId = req.params.id;
      
        user_game.update({
          name: req.body.name,
          email: req.body.email,
          credit: req.body.credit
        }, {
          where: {
            id: userId
          }
        }).then(user => {
          user_game_data.update({
            alamat: req.body.alamat,
            kelamin: req.body.kelamin
          }, {
            where: {
              user_game_id: userId
            }
          }).then(biodata => {
            user_game_history.update({
                win: req.body.win,
                lose: req.body.lose
            }, {
                where:{
                    id: userId
                }
            }).then(view => {
                res.redirect(301, '/dashboard')
            })
          })
        })
      }
}