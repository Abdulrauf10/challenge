'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
   await queryInterface.bulkInsert('user_game_data', [{
    user_game_id: 1,
    alamat: 'duomo',
    kelamin: 'pria',
    createdAt: new Date(),
    updatedAt: new Date()
   }])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('user_game_data', null, {})
  }
};

